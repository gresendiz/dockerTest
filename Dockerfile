FROM ubuntu:trusty  
MAINTAINER Guillermo Resendiz <guillermo.resendiz@openjawtech.com>

RUN apt-get update && \  
    apt-get install -y mariadb-server pwgen


RUN sed -i -r 's/bind-address.*$/bind-address = 0.0.0.0/' /etc/mysql/my.cnf  
RUN sed -i -r 's/port.*$/port = 3305'/ /etc/mysql/my.cnf

ADD createuser.sh /createuser.sh  
ADD runmariadb.sh /runmariadb.sh 
ADD tablas.sql /tablas.sql 
ADD start.sh /start.sh
RUN chmod 775 /*.sh


RUN /bin/bash -c "source /runmariadb.sh"
CMD bash -c '/start.sh';'bash'
